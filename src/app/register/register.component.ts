import { Component } from "@angular/core";
import { Router} from "@angular/router";
import { User} from "../entity/user"
import { LoginComponent} from "../login/login"


@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent {
  email="";
  password="";
  confirmPassword="";
  error="";

  constructor(private router:Router) {}
  
  login=new LoginComponent();

  register(email:any,password:any,confirmPassword:any) {
    if(password==confirmPassword){
      this.router.navigate(['/inicio']);
      this.error="";
    }else{
      this.error="las contraseñas no coinciden";
    }
  }

}