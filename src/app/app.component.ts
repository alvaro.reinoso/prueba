import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  testArray=new Array<String>();
  //testForm: new FormGroup()

  addTest(value:any){
    if(value!==""){
     this.testArray.push(value)
    //console.log(this.tests) 
  }else{
    alert('Field required **')
  }
    
  }

  //delete item
  deleteItem(test:any){
  	for(let i=0 ;i<= this.testArray.length ;i++){
  		if(test== this.testArray[i]){
  			this.testArray.splice(i,1)
  		}
  	}
  }

  // submit Form
  testSubmit(value:any){
     if(value!==""){
       this.testArray.push(value.test)
       //this.testForm.reset()
    }else{
      alert('Field required **')
    }
    
  } 
}
