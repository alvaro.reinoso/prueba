-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-08-2021 a las 14:43:25
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `archivo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo_istitucional`
--

CREATE TABLE IF NOT EXISTS `archivo_istitucional` (
  `id_arch` int(11) NOT NULL AUTO_INCREMENT,
  `normas_juridicas` varchar(20) NOT NULL,
  `series_documentales` varchar(20) NOT NULL,
  `series_documentales_ministeio` varchar(20) NOT NULL,
  `tabla_retencion` varchar(20) NOT NULL,
  `cuadro_clasificacion` varchar(20) NOT NULL,
  `comision` varchar(20) NOT NULL,
  `local` varchar(20) NOT NULL,
  `condicones_local` varchar(20) NOT NULL,
  `tranferencias_archivo_central` varchar(20) NOT NULL,
  `otros_archivo` varchar(20) NOT NULL,
  `plan_conservacion` varchar(20) NOT NULL,
  `plan_reduccion` varchar(20) NOT NULL,
  `resolucciones` varchar(20) NOT NULL,
  `plan_economia` varchar(20) NOT NULL,
  `fechas` varchar(20) NOT NULL,
  `analisis_implementacion` varchar(20) NOT NULL,
  `frecuencia_analisis` varchar(20) DEFAULT NULL,
  `geforza` varchar(20) NOT NULL,
  `anno_geforza` varchar(20) DEFAULT NULL,
  `cant_geforza` int(20) DEFAULT NULL,
  PRIMARY KEY (`id_arch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuadro`
--

CREATE TABLE IF NOT EXISTS `cuadro` (
  `id_cuad` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(20) NOT NULL,
  `cargo` varchar(20) NOT NULL,
  `designado` varchar(20) NOT NULL,
  `nivel_escolar` varchar(20) NOT NULL,
  `especialidad_graduado` varchar(20) NOT NULL,
  `categoria_cientifica` varchar(20) NOT NULL,
  `categoria_docente` varchar(20) NOT NULL,
  `capacitaciones_recibidas` varchar(100) NOT NULL,
  PRIMARY KEY (`id_cuad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuadro`
--

INSERT INTO `cuadro` (`id_cuad`, `nombre`, `telefono`, `correo`, `cargo`, `designado`, `nivel_escolar`, `especialidad_graduado`, `categoria_cientifica`, `categoria_docente`, `capacitaciones_recibidas`) VALUES
(1, 'nombre', 'telefono', 'correo', 'cargo', 'designado', 'nivel_escolar', 'especialidad_graduad', 'categoria_cientifica', 'categoria_docente', 'capacitaciones_recibidas'),
(3, 'nombre cambiado2', 'telefono', 'correo', 'cargo', 'designado', 'nivel_escolar', 'especialidad_graduad', 'categoria_cientifica', 'categoria_docente', 'capacitaciones_recibidas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `id_dir` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_dir`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `director`
--

INSERT INTO `director` (`id_dir`, `nombre`, `telefono`, `correo`) VALUES
(1, 'nombre', 'telefono', 'correo'),
(3, 'nombre_cambiado', 'telefono', 'correo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

CREATE TABLE IF NOT EXISTS `general` (
  `id_istitut` int(11) NOT NULL AUTO_INCREMENT,
  `organismo` varchar(20) NOT NULL,
  `municipio` int(11) NOT NULL,
  `entidad` varchar(20) NOT NULL,
  `subordinacion` varchar(20) NOT NULL,
  `director` int(11) NOT NULL,
  `cuadro` int(11) NOT NULL,
  `archivo_institucional` int(11) NOT NULL,
  PRIMARY KEY (`id_istitut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
