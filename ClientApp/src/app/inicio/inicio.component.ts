import { Component, OnInit } from '@angular/core';


import { Archivo } from "../entity/archivo"

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  public archivos: Archivo[] = [
    new Archivo("CITMA", "Santa Clara", "CITMATEL", "Local", "Pedro Pérez Pérez", "Raul Gaga Pool"),
    new Archivo("MINCOM", "Ranchuelo", "ETECSA", "Nacional", "Carlos Ruíz Roy", "Rene Real Delgado"),
    new Archivo("ANAP", "Manicaragua", "ANAP Manicaragua", "Local", "Marcos Lois Laz", "Samir Had Naf"),
    new Archivo("FAR", "Corralillo", "Estado Mayor Corralillo", "Local", "José Hernádez Giménez", "Gerald Pool Faifer"),
    new Archivo("MININT", "Santa Clara", "Delegación VC ", "Provincial", "Rodolfo Gutierres Pérez", "Tom Brows Room"),
    new Archivo("CITMA", "Santa Clara", "CITMAVC", "Provincial", "Juan Pérez Pérez","Cris Rodriguez"),
    new Archivo("CITMA", "Remedios", "CITMAVC  Remedios", "Local", "Manolo Kay Ruís","Carlos Bentacourt")
  ];
  public archivosReturn = this.archivos;

  constructor() { }

  public munic:String="Todos";

  eliminarArchivo(archivo: Archivo) {
    var index1 = this.archivos.indexOf(archivo);
    if (index1 !== -1) {
      this.archivos.splice(index1, 1);
    }
    this.munArchivos(this.munic);    
  }
  munArchivos(mun: String) {
    if (mun == "todos") {
      this.archivosReturn = this.archivos;
    } else {
      this.archivosReturn = [];
      for (let index = 0; index < this.archivos.length; index++) {
        if (this.archivos[index].municipio == mun) {
          this.munic=mun;
          this.archivosReturn.push(this.archivos[index]);          
        }
      }
    }
  }

  ngOnInit() {
    this.obtenerArchivos();
  }

  obtenerArchivos() {
    return this.archivos;
  }
}
