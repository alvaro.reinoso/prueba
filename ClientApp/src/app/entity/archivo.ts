export class Archivo {
    constructor(
        public organismo: string,
        public municipio: string,
        public entidad: string,
        public subordinacion: string,
        public director: string,    
        public cuadro: string, 
        public id?: number,
    ) { }

}
