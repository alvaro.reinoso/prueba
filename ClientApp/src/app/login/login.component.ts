import { Component } from "@angular/core";
import { Router} from "@angular/router";
import { User} from "../entity/user"

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})



export class LoginComponent { 

  constructor(
    private router:Router,
    ) {}
  
  public users: User[] = [
    new User("alvaro.reinoso@ntsprint.com","123456"),
    new User("alvaro@ntsprint.com","123")
  ];
  error="";
  getUser(){
    return this.users;
  }

  login(email:any,password:any) {
    var u=null;
    for (let index = 0; index < this.users.length; index++) {
      if (this.users[index].email== email) {
        u=this.users[index];
      }
    }
    if(u!=null){
      if(u.password==password){
        this.router.navigate(['/inicio']);
        this.error="";
      }else{
        this.error="contraseña incorrecta";
      }
    }else{
      this.error="este usuario no existe";
    }
  }
  register(){
    this.router.navigate(['/register']);
  }
}