using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Data;

namespace proyecto
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try{
                MySqlConnection sqlCon = new MySqlConnection();
                sqlCon.ConnectionString="Server=localhost; database=archivo; UID=root; password=; SslMode = none";
                sqlCon.Open();
                //insertCuadro(sqlCon,3, "nombre", "telefono", "correo", "cargo", "designado", "nivel_escolar", "especialidad_graduado", "categoria_cientifica", "categoria_docente", "capacitaciones_recibidas");
                //deleteCuadro(sqlCon,2);
                //updateCuadro(sqlCon,3, "nombre cambiado2", "telefono", "correo", "cargo", "designado", "nivel_escolar", "especialidad_graduado", "categoria_cientifica", "categoria_docente", "capacitaciones_recibidas");
                
                //insertDirector(sqlCon,2, "nombre", "telefono", "correo");
                //deleteDirector(sqlCon,2);
                //updateDirector(sqlCon,3, "nombre_cambiado", "telefono", "correo");

                if(sqlCon!=null){
                    sqlCon.Close();
                    Console.WriteLine("siii");
                }
            }catch(Exception ex){
                Console.WriteLine("nooooo", ex);  
            }            
            //CreateHostBuilder(args).Build().Run();
        }
        
        
        /**CUADRO**/
        public static void insertCuadro(MySqlConnection sqlCon, int id_cuad, string nombre, string telefono, string correo, string cargo, string designado, string nivel_escolar, string especialidad_graduado, string categoria_cientifica, string categoria_docente, string capacitaciones_recibidas){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="INSERT INTO cuadro VALUES ('"+id_cuad+"','"+nombre+"', '"+telefono+"', '"+correo+"', '"+cargo+"', '"+designado+"', '"+nivel_escolar+"', '"+especialidad_graduado+"', '"+categoria_cientifica+"', '"+categoria_docente+"', '"+capacitaciones_recibidas+"');";
            cmd.ExecuteNonQuery();
        }
        public static void updateCuadro(MySqlConnection sqlCon, int id_cuad, string nombre, string telefono, string correo, string cargo, string designado, string nivel_escolar, string especialidad_graduado, string categoria_cientifica, string categoria_docente, string capacitaciones_recibidas){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="UPDATE cuadro SET nombre='"+nombre+"', telefono='"+telefono+"', correo='"+correo+"', cargo='"+cargo+"', designado='"+designado+"', nivel_escolar='"+nivel_escolar+"', especialidad_graduado='"+especialidad_graduado+"', categoria_cientifica='"+categoria_cientifica+"', categoria_docente='"+categoria_docente+"', capacitaciones_recibidas='"+capacitaciones_recibidas+"' WHERE id_cuad='"+id_cuad+"';";
            cmd.ExecuteNonQuery();
        }
        public static void deleteCuadro(MySqlConnection sqlCon, int id_cuad){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="DELETE FROM cuadro WHERE id_cuad='"+id_cuad+"';";
            cmd.ExecuteNonQuery();
        }

        /**DIRECTOR**/
        public static void insertDirector(MySqlConnection sqlCon, int id_dir, string nombre, string telefono, string correo){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="INSERT INTO director VALUES ('"+id_dir+"','"+nombre+"', '"+telefono+"', '"+correo+"');";
            cmd.ExecuteNonQuery();
        }
        public static void updateDirector(MySqlConnection sqlCon, int id_dir, string nombre, string telefono, string correo){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="UPDATE director SET nombre='"+nombre+"', telefono='"+telefono+"', correo='"+correo+"' WHERE id_dir='"+id_dir+"';";
            cmd.ExecuteNonQuery();
        }
        public static void deleteDirector(MySqlConnection sqlCon, int id_dir){
            MySqlCommand cmd=new MySqlCommand();
            cmd.Connection=sqlCon;
            cmd.CommandText="DELETE FROM director WHERE id_dir='"+id_dir+"';";
            cmd.ExecuteNonQuery();
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}